#!/usr/bin/env python -i

import argparse
from datetime import datetime,date, timedelta
import os
import numpy as N
from PIL import Image
import glob
import moviepy.editor as mpy

wgetTemplate="wget -O %s %s"
#You can change this depending on where you are pulling the images from
urlTemplate="ftp://ftp.nnvl.noaa.gov/GOES/ABI_GeoColor/%s_GOES16-ABI-FD-GEOCOLOR-5424x5424.jpg"
#In the case of the GOES-16 images, these numbers correspond to the image numbers
#For the GOES-16 images, it is imperative that the first number in the range is a multiple of 100

#For vis_range, this is the range of images you want to pull
#Example: vs_range = N.arange(20181820300,20181830300,100)
vis_range = N.arange()
#this add value range corresponds to how the GOES-16 images are sequenced on the website
add = N.arange(0,46,15)
for i in range(len(vis_range)):
    image=vis_range[i] + add
    #this creates a set of 4 unique image sequence numbers that will get pulled
    for j in image:
        file_name = "image_" + str(j) + ".jpg"
        url=urlTemplate % (j)
        wgetCmmnd = wgetTemplate % (file_name, url)
        os.system("echo " + wgetCmmnd)
        os.system(wgetCmmnd)
        if os.stat("image_"+str(j)+".jpg") == 0:
            os.remove("image_"+str(j)+".jpg")
            #this if statement tests if an image that has been pulled contains data, if it doesn't
            #it gets deleted
        else:
            pass
        try:
            images = Image.open('image_'+str(j)+'.jpg')
            #Once the image has been opened, you can either save it directly or manipulate it
            #The ones I use most frequently are crop and resize
            #this crop is set up to capture dust off of Africa, and the arguements correspond
            # to: left boundary, upper boundary, right boundary, bottom boundary
            #this will use PIL and Image to open the image, crop it, and then save the new image
            new_image = images.crop((3500,1500,5424,2500))
            new_image.save('image_'+str(j)+'.jpeg')
            # or you can resize it
            new_image = images.resize((1000,1000))
            new_image.save('image_'+str(j)+'.jpeg')
        except(OSError):
            continue
#need to set the dir_name prior to running, this is the directory where you save the images
            #and then this will remove all of the initial .jpg images you pulled
            
dir_name=
test1 = os.listdir(dir_name)
for items in test1:
    if items.endswith(".jpg"):
        os.remove(os.path.join(dir_name, items))


mp4_name = '' #insert whatever name you want it to be
#set your frames per second
fps=12
file_list1 = glob.glob('*.jpeg')
list.sort(file_list1, key=lambda x: int(x.split('_')[1].split('.jpeg')[0]))
clip = mpy.ImageSequenceClip(file_list1, fps=fps)
clip.write_videofile('{}.mp4'.format(mp4_name), fps=fps)
#to create a GIF see next line
#clip.write_gif('{}.gif'.format(mp4_name), fps=fps)

#again, set the directory name so that all after the movie has been created, the jpegs will be deleted so that
#your directory doesn't fill up as the images are quite large
dir_name =
test = os.listdir(dir_name)
for item in test:
    if item.endswith(".jpeg"):
        os.remove(os.path.join(dir_name, item))